/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beadandomestint;

import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 *
 * @author Zsolt
 */
public class Tabla {

    int[][] tabla;

    public Tabla() {
        this(7, 6);
    }

    public Tabla(int width, int height) {
        tabla = new int[height][width];
    }

    public Tabla clone() {
        Tabla gb = new Tabla(tabla[0].length, tabla.length);
        gb.tabla = masolat(tabla);
        return gb;
    }

    public int getWidth() {
        return tabla[0].length;
    }

    public int getHeight() {
        return tabla.length;
    }

    public int getLocation(int sor, int oszlop) {
        return tabla[sor][oszlop];
    }

    public void setLocation(int sor, int oszlop, int ertek) {
        tabla[sor][oszlop] = ertek;
    }

    public boolean dropDisc(int oszlop, int player) {
        for (int row = tabla.length - 1; row >= 0; row--) {
            if (tabla[row][oszlop] == 0) {
                setLocation(row, oszlop, player);
                return true;
            }
        }
        return false;
    }

    public int checkWin() {
        for (int sor = 0; sor < tabla.length; sor++) {
            for (int oszlop = 0; oszlop < tabla[0].length; oszlop++) {
                int player = tabla[sor][oszlop];
                if (player == 0) {
                    continue;
                }
                if (sor + 3 < tabla.length && tabla[sor + 1][oszlop] == player && tabla[sor + 2][oszlop] == player && tabla[sor + 3][oszlop] == player) {
                    return player;
                }
                if (oszlop + 3 < tabla[0].length && tabla[sor][oszlop + 1] == player && tabla[sor][oszlop + 2] == player && tabla[sor][oszlop + 3] == player) {
                    return player;
                }
                if ((sor + 3 < tabla.length && oszlop + 3 < tabla[0].length)
                        && tabla[sor + 1][oszlop + 1] == player && tabla[sor + 2][oszlop + 2] == player && tabla[sor + 3][oszlop + 3] == player) {
                    return player;
                }
                if ((sor - 3 > 0 && oszlop + 3 < tabla[0].length)
                        && tabla[sor - 1][oszlop + 1] == player && tabla[sor - 2][oszlop + 2] == player && tabla[sor - 3][oszlop + 3] == player) {
                    return player;
                }
            }
        }
        return -1;
    }

    /////
    public void isWin() {
        String s1 = "NYERTÉL!";
        String s2 = "VESZÍTETTÉL!";

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                if (tabla[i][j] == 1) {
                    if (i-3>0 && tabla[i - 1][j] == 1 && tabla[i - 2][j] == 1 && tabla[i - 3][j] == 1) {
                        JOptionPane.showMessageDialog(null, s1);

                    }
                } else if (tabla[i][j] == -1) {
                    if (i-3>0 && tabla[i - 1][j] == -1 && tabla[i - 2][j] == -1 && tabla[i - 3][j] == -1) {
                        JOptionPane.showMessageDialog(null, s2);

                    }
                }

            }

        }

        //vízszintes ellenőrzés
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 4; j++) {
                if (tabla[i][j] == 1) {
                    if (j+3<=7 && tabla[i][j + 1] == 1 && tabla[i][j + 2] == 1 && tabla[i][j + 3] == 1) {
                        JOptionPane.showMessageDialog(null, s1);

                    }
                } else if (tabla[i][j] == -1) {
                    if (j+3<=7 && tabla[i][j + 1] == -1 && tabla[i][j + 2] == -1 && tabla[i][j + 3] == -1) {
                        JOptionPane.showMessageDialog(null, s2);

                    }
                }

            }

        }

        //átlós ellenőrzés
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                if (tabla[i][j] == 1) {
                    if (j+3<=7 && i+3<=6 && tabla[i + 1][j + 1] == 1 && tabla[i + 2][j + 2] == 1 && tabla[i + 3][j + 3] == 1) {
                        JOptionPane.showMessageDialog(null, s1);

                    }
                } else if (tabla[i][j] == -1) {
                    if (j+3<=7 && i+3<=6 && tabla[i + 1][j + 1] == -1 && tabla[i + 2][j + 2] == -1 && tabla[i + 3][j + 3] == -1) {
                        JOptionPane.showMessageDialog(null, s2);

                    }
                }

            }

        }

        //átlós ellenőrzés másik oldalra
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                if (tabla[i][j] == 1) {
                    if (i>=3 && tabla[i - 1][j - 1] == 1 && tabla[i - 2][j - 2] == 1 && tabla[i - 3][j - 3] == 1) {
                        JOptionPane.showMessageDialog(null, s1);

                    }
                } else if (tabla[i][j] == -1) {
                    if (i>=3 && tabla[i - 1][j - 1] == -1 && tabla[i - 2][j - 2] == -1 && tabla[i - 3][j - 3] == -1) {
                        JOptionPane.showMessageDialog(null, s2);

                    }
                }

            }

        }

    }

    /////
    public int[][] masolat(int[][] src) {
        int dest[][] = new int[src.length][];
        for (int i = 0; i < src.length; i++) {
            dest[i] = Arrays.copyOf(src[i], src[i].length);
        }
        return dest;
    }

}
