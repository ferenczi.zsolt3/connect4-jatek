/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beadandomestint;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 *
 * @author Zsolt
 */
public class Jatekmenet extends JFrame implements ActionListener {

  
    Tabla t = new Tabla();
    private static JLabel slots[][] = new JLabel[6][7];
    private JButton gombok[] = new JButton[7];
    Ai ai = new Ai();

    public Jatekmenet() {
        JFrame f = new JFrame("Beadandó program");
        JPanel panel = (JPanel) f.getContentPane();
        panel.setLayout(new GridLayout(7, 7));

        for (int i = 0; i < 7; i++) {
            gombok[i] = new JButton(i + 1 + " Gomb");
            gombok[i].addActionListener(this);
            panel.add(gombok[i]);
        }

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                slots[i][j] = new JLabel();
                slots[i][j].setBorder(new LineBorder(Color.black));
                panel.add(slots[i][j]);
            }
        }

        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setContentPane(panel);
        f.setSize(700, 700);
        f.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object s = e.getSource();
        for (int i = 0; i < 7; i++) {
            if (s == gombok[i]) {
                gombonKattint(i);
            }

        }
    }

    public void gombonKattint(int i) {      
        t.dropDisc(i, 1);       
        frissit();
        t.isWin();
        int s = ai.getBestMove(t);
        t.dropDisc(s, -1);
        frissit();
        t.isWin();
        ai.kiir(t);
        
     
        
        

    }

    public void frissit() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                if (t.getLocation(i, j) == 1) {
                    slots[i][j].setIcon(new ImageIcon("red.gif"));
                } else if (t.getLocation(i, j) == -1) {
                    slots[i][j].setIcon(new ImageIcon("blue.gif"));
                }
            }
        }

    }
    

}
