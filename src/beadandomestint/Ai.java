/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beadandomestint;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Zsolt
 */
public class Ai {

   

    Random r = new Random();
    int melyseg = 6;
    Tabla t;
    int player=-1;
/*
    public int getBestMove(Tabla state) {
        int results[] = minimax(state, this.player, melyseg);
       
        return results[1];
    }
    */

    
    public int getBestMove(Tabla state) {
        int results[] = minimax(state, 1, melyseg);
        
        return results[1];
        // return r.nextInt(7); //véletlen szám generálással tesztelni a "helyes" működést//
        //return algoritmus(state);
    }

    private boolean gameOver(Tabla state) {
        if (state.checkWin() >= 0) {
            return true;
        }
        return false;
    }

    public boolean tele(Tabla t) {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                if (t.getLocation(j, j) == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public void kiir(Tabla tabla) {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(tabla.getLocation(i, j) + " ");
            }
            System.out.print("\n");
        }
        System.out.println("\n");
    }

    public int algoritmus(Tabla tabla) {
        List<Tabla> l = new ArrayList();
        int t[] = new int[10];
        Tabla masolat = tabla.clone();

        l.add(masolat);

        for (int i = 0; i < 10; i++) {

            int aidob = r.nextInt(7);
            l.get(0).dropDisc(aidob, -1);
            t[i] = aidob;
            l.get(0).dropDisc(r.nextInt(7), 1);
            if (l.get(0).checkWin() < 0) {
                return t[0];
            }

        }

        return t[0];
    }
    
    
    public int[] minimax(Tabla state, int player, int melyseg) {
	
		if (gameOver(state) || melyseg == 0) {
	
				
			return new int[]{kiertekeles(state, melyseg), 0};
		}
		if (player == this.player) {
			int max = Integer.MIN_VALUE;
			int max_col = 0;
			for (int col = 0; col<state.getWidth(); col++) {
				Tabla next = state.clone();
				if (!next.dropDisc(col, player)) continue; 
				int val = minimax(next, 1, melyseg-1)[0];

				if (val > max) {
					max = val;
					max_col = col;
				}
			}
			
			return new int[]{max,max_col};
		} else {
			int min = Integer.MAX_VALUE;
			int min_col = 0;
			for (int col = 0; col<state.getWidth(); col++) {
				Tabla next = state.clone();
				if (!next.dropDisc(col, player)) continue;
				int val = minimax(next, -1, melyseg-1)[0];
				if (val < min) {
					min = val;
					min_col = col;
				}
			}
			return new int[]{min, min_col};
		}

    }
    
    private int kiertekeles(Tabla state, int melyseg) {
        
        int c=0;
		if (state.checkWin() > 0) {
		
			if (state.checkWin() == this.player) {
		
				return 100000 + melyseg;
			} else {
				return -100000 - melyseg; 
			}
		}
                else{                                   
                    for(int i=0;i<7;i++){
                        Tabla next = state.clone();
                        next.dropDisc(i, 1);
                        if(state.checkWin()>1){
                            c=i;
                        }
                    }
                }
                return c;
                //else return r.nextInt(7);         //kijavítani!!
}

}

